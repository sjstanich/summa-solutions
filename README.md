**Test Summa Solutions**

Crear las clases necesarias para administrar los Empleados de una Empresa. Cada empleado puede ser de distinto tipo como ser, Programador, Diseñador.

---

## Estructura de clases: 

**Empresa**
1. id
2. nombre
3. empleados

**Programador**
1. id
2. nombre
3. apellido
4. edad
5. lenguaje en el que programa (PHP,PYTHON, NET)

**Diseñador**
1. id
2. nombre
3. apellido
4. edad
5. tipo de diseñador (grafico o web)

En **Empresa** tengo que poder:
1. Agregar **Empleados**
2. Obtener un listado de todos los **Empleados**
3. Buscar por **ID** de **Empleado**
4. Obtener un promedio de edad de los empleados

**Nota**: Demostrar conocimientos en el manejo de objetos, getters, setters, listados y herencia. 
