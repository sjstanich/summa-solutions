<?php


class empleado
{
    private $id;
    private $edad;
    private $puesto;
    private $nombre;
    private $apellido;

    /**
     * @param int $id id del empleado
     * @param int $edad edad del empleado
     * @param string $puesto puesto donde se desempeña
     * @param string $nombre nombre del empleado
     * @param string $apellido apellido del empleado
     */

    public function __construct($id, $edad, $puesto, $nombre, $apellido)
    {
        $this->id = $id;
        $this->edad = $edad;
        $this->puesto = $puesto;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return string
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @param int $edad
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;
    }

    /**
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @param string $puesto
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;
    }
}