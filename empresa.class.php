<?php

class empresa
{
    private $id;
    private $empresa;
    private $empleados = array();

    /**
     * @param int $id id de la empresa
     * @param string $empresa nombre de la empresa
     */

    public function __construct ($id, $empresa)
    {
        $this->id = $id;
        $this->empresa = $empresa;
    }

    /**
     * @param object $empleado
     */

    public function addEmpleado ($empleado)
    {
            $this->empleados[] = $empleado;
    }

    /**
     * @return array empleados
     */

    public function getEmpleados ()
    {
        return $this->empleados;
    }

    /**
     * @param int $id id del empleado
     * @return object $empleado empleado
     */

    public function getEmpleadoById ($id)
    {
        foreach ($this->empleados as $empleado)
        {
            if ($empleado->getId() === $id)
                return $empleado;
        }
    }

    /**
     * @return integer promedio de edad
     */

    public function getEdadPromedio ()
    {
        $edad = 0;
        foreach ($this->empleados as $empleado){
            $edad += $empleado->getEdad();
        }

        return round($edad / count($this->empleados), 0 );
    }
}