<?php
include 'empresa.class.php';
include 'empleado.class.php';
include 'programador.class.php';
include 'disenador.class.php';


$empresa = new Empresa(1, 'Test Summa' );


$empresa->addEmpleado(new programador(1, 42, 'PHP', 'Sebas', 'Stanich' ));
$empresa->addEmpleado(new disenador(2, 32, 'WEB', 'Francisco', 'Luna'));
$empresa->addEmpleado(new programador(3, 38, 'PYTHON', 'Jose', 'Perez'));
$empresa->addEmpleado(new programador(4, 45, 'NET', 'Pablo', 'Lorentti'));
$empresa->addEmpleado(new disenador(5, 25, 'GRAFICO', 'Fito', 'Paez'));

// Recorremos todos los empleados
foreach ($empresa->getEmpleados() as $empleado){
    echo "Empleado ID:". $empleado->getId() ." Nombre y Apellido:". $empleado->getNombre() ." ". $empleado->getApellido() ." Edad:". $empleado->getEdad() ." Puesto:". get_class($empleado) ." ". $empleado->getPuesto();
    echo "<br/>";
    echo "<br/>";
}

// buscamos empleado x id
echo "Buscamos por Id de empleado ";
$empleado = $empresa->getEmpleadoById(1);
echo "<br/> ID:". $empleado->getId() ." Nombre y Apellido:". $empleado->getNombre() ." ". $empleado->getApellido() ." Edad:". $empleado->getEdad() ." Puesto:". get_class($empleado) ." ". $empleado->getPuesto();
echo "<br/>";

$empleado = $empresa->getEmpleadoById(3);
echo "<br/> ID:". $empleado->getId() ." Nombre y Apellido:". $empleado->getNombre() ." ". $empleado->getApellido() ." Edad:". $empleado->getEdad() ." Puesto:". get_class($empleado) ." ". $empleado->getPuesto();
echo "<br/>";

echo "<br/>";
// Promedio de edad
echo "Edad promedio de la empresa ". $empresa->getEdadPromedio() ." años";

